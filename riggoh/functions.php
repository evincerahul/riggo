<?php
/**
 * riggoh_company functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package riggoh_company
 */

if ( ! function_exists( 'riggoh_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function riggoh_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on riggoh_company, use a find and replace
		 * to change 'riggoh' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'riggoh', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'riggoh' ),
		) );

		function register_my_menus() {
  register_nav_menus(
    array(
      'new-menu' => __( 'New Menu' ),
      'footer' => __( 'footer' ),
      'privacy' => __( 'privacy' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'riggoh_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'riggoh_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function riggoh_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'riggoh_content_width', 640 );
}
add_action( 'after_setup_theme', 'riggoh_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function riggoh_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'riggoh' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'riggoh' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'riggoh_widgets_init' );

//CUSTOM WIDGET
register_sidebar( array(
'name' => 'footer site logo',
'id' => 'f_site_logo',
'description' => 'Footer Site logo',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );


register_sidebar( array(
'name' => 'Contact Us',
'id' => 'contact_us',
'description' => 'Contact us',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );


register_sidebar( array(
'name' => 'Menu 1',
'id' => 'menu_1',
'description' => 'Menu 1',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );


register_sidebar( array(
'name' => 'Menu 2',
'id' => 'menu_2',
'description' => 'Menu 2',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );


register_sidebar( array(
'name' => 'Social icon or link',
'id' => 'icon_link',
'description' => 'Social icon || Copyright || link',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

register_sidebar( array(
'name' => 'Terms|Privacy',
'id' => 'terms_privacy',
'description' => 'Terms|Privacy',
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

/**
 * Enqueue scripts and styles.
 */


function riggoh_scripts() {
	wp_enqueue_style( 'riggoh-style', get_stylesheet_uri() );

	wp_enqueue_script( 'riggoh-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'riggoh-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'riggoh_scripts' );

function additional_custom_styles() {

    /*Enqueue The Styles*/
    wp_enqueue_style( 'uniquestylesheetid', get_template_directory_uri() . '/assets/css/custom.css' ); 
    wp_enqueue_style( 'responsive_stylesheet', get_template_directory_uri() . '/assets/css/responsive.css' ); 
     // wp_enqueue_style( 'riggoh-stylesheet', get_template_directory_uri() . '/assets/css/blocks.css' ); 
}
add_action( 'wp_enqueue_scripts', 'additional_custom_styles' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


// ---------------------------------
// Added By Us

function testimonial() {
    // set up product labels
    $labels = array(
        'name' => 'Testimonial',
        'singular_name' => 'Testimonial',
        'add_new' => 'Add New ',
        'add_new_item' => 'Add New Testimonial',
        'edit_item' => 'Edit Testimonial',
        'new_item' => 'New Testimonial',
        'all_items' => 'All Testimonials',
        'view_item' => 'View Testimonials',
        'search_items' => 'Search Testimonials',
        'not_found' =>  'No Testimonials Found',
        'not_found_in_trash' => 'No Testimonials found in Trash', 
        'parent_item_colon' => '',
        'menu_name' => 'Testimonials',
    );
    
    // register post type
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'testimonial'),
        'query_var' => true,
        'menu_icon' => 'dashicons-thumbs-up',
        'supports' => array(
            'title',
            'editor',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes'
        )
    );
    register_post_type( 'testimonial', $args );
}
add_action( 'init', 'testimonial' );

// Register Custom Taxonomy for solutions
function testimonial_category() {
		$labels = array(
			'name'                       => _x( 'Testimonial Categories ', 'Taxonomy General Name', 'text_domain' ),
			'singular_name'              => _x( 'Testimonial Categories', 'Taxonomy Singular Name', 'text_domain' ),
			'menu_name'					 => __( 'Testimonial Categories', 'text_domain' ),
			'all_items' 				 => __( 'All Categories', 'text_domain' ),
			'parent_item'  				 => __( 'Parent Item', 'text_domain' ),
			'parent_item_colon'			 => __( 'Parent Item:', 'text_domain' ),
			'new_item_name'				 => __( 'New Testimonial Categories', 'text_domain' ),
			'add_new_item'				 => __( 'Add Testimonial Categories', 'text_domain' ),
			'edit_item'					 => __( 'Edit Testimonial Categories', 'text_domain' ),
			'update_item'				 => __( 'Update Testimonial Categories', 'text_domain' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
			'search_items'               => __( 'Search Categories', 'text_domain' ),
			'add_or_remove_items'        => __( 'Add or remove Testimonial Categories', 'text_domain' ),
			'choose_from_most_used'      => __( 'Choose from the most used items', 'text_domain' ),
			'not_found'                  => __( 'Not Found', 'text_domain' ),
		);

		$args = array(
			'label' 			=> 'Testimonials Category',
			'labels' 			=> $labels,
			'hierarchical' 		=> true,
			'public' 			=> true,
			'show_ui' 			=> true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' 	=> true,
			'query_var' 		=> true
		);
	register_taxonomy( 'testimonialcat', 'testimonial', $args );
}

add_action( 'init', 'testimonial_category', 0 );

function custom_text_validation_filter($result, $tag) {  
      $type = $tag['type'];  
      $name = $tag['name'];
      //here textbox type name is 'subject'
      if($name == 'firstname'|| 'lastname') { 
          $value = $_POST[$name];  
          if (preg_match('/[\'^£$%&*()}{@#~><>|=_+¬]/', $value)){
            $result->invalidate( $tag, "Invalid characters." );
          } 
      } 
      return $result;  
}
add_filter('wpcf7_validate_text','custom_text_validation_filter', 10, 2); 
add_filter('wpcf7_validate_text*', 'custom_text_validation_filter', 10, 2); 

/* Code for fields in General Setting 
**Starts here
*/
add_filter('admin_init', 'general_settings_register_fields');

function general_settings_register_fields()
{
	register_setting('general', 'timeframedays', 'esc_attr');
	register_setting('general', 'commissionpercentage', 'esc_attr');
	add_settings_field('timeframedays', '<label for="timeframedays">'.__('Time Frame Last X days' , 'timeframedays' ).'</label>' , 'general_settings_time_frame', 'general');
	add_settings_field('commissionpercentage', '<label for="commissionpercentage">'.__('Commission margin (in %)' , 'commissionpercentage' ).'</label>' , 'general_settings_commission_margin', 'general');
}

function general_settings_time_frame()
{
	$value = get_option( 'timeframedays', '' );
	echo '<input type="number" id="timeframedays" name="timeframedays" value="' . $value . '" />';
}

function general_settings_commission_margin()
{
    $value1 = get_option( 'commissionpercentage', '' );
	 echo '<input type="number" id="commissionpercentage" name="commissionpercentage" value="' . $value1 . '" />';
}

function filter_plugin_updates( $value ) {
    unset( $value->response['testimonials/index.php'] );
    return $value;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );

/* Ends here */