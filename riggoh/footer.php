<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package riggoh_company
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<!-- <a href="<?php //echo esc_url( __( 'https://wordpress.org/', 'riggoh' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				//printf( esc_html__( 'Proudly powered by %s', 'riggoh' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				//printf( esc_html__( 'Theme: %1$s by %2$s.', 'riggoh' ), 'riggoh', '<a href="https://riggoh.com/">Riggoh</a>' );
				?> -->
				<div class="footer_wrapper">
				<div class="footer_column footer_logo">
				<a href="<?php echo get_site_URL();?>">
				<?php
				if(is_active_sidebar('f_site_logo')){
				dynamic_sidebar('f_site_logo');
				}
				?>
				</a>
				</div>
				<div class="footer_column">
				<a class="single_menu" href="<?php echo get_site_URL();?>/contact/">Contact us</a>
				<?php
				if(is_active_sidebar('contact_us')){
				dynamic_sidebar('contact_us');
				}
				?>
				</div>
				<div class="footer_column menu_combine">
				<?php
				if(is_active_sidebar('menu_1')){
				dynamic_sidebar('menu_1');
				}
				?>
				</div>
				<div class="footer_column menu_combine _menu_footer">
				<?php
				if(is_active_sidebar('menu_2')){
				dynamic_sidebar('menu_2');
				}
				?>
				</div>

				<div class="footer_column icon_or_link">
				<div class="copyright_info">
				<?php
				if(is_active_sidebar('icon_link')){
				dynamic_sidebar('icon_link');
				}
				?>
				<p>copyright &copy; <?php echo date('Y'); echo " "; echo get_bloginfo('name'); ?></p> 	
				<?php
				if(is_active_sidebar('terms_privacy')){
				dynamic_sidebar('terms_privacy');
				}
				?>		
				</div>


				</div>
				</div>

				<div class="responsive_footer" style="display: none;">
					<div class="footer_part">
					<div class="footer_devider">
						<div class="footer_column menu_combine">
						<?php
						if(is_active_sidebar('menu_1')){
						dynamic_sidebar('menu_1');
						}
						?>
						</div>
					</div>
					<div class="footer_devider">
						<div class="footer_column menu_combine _menu_footer">
						<?php
						if(is_active_sidebar('menu_2')){
						dynamic_sidebar('menu_2');
						}
						?>
						</div>

						<div class="footer_column">
						<a class="single_menu" href="<?php echo get_site_URL();?>/contact/">Contact us</a>
						<?php
						if(is_active_sidebar('contact_us')){
						dynamic_sidebar('contact_us');
						}
						?>
						</div>
					</div>
					</div>
					<div class="footer_part footer_bottom">
					<div class="footer_devider">
						<div class="footer_column res_logo">
						<a href="<?php echo get_site_URL();?>">
						<?php
						if(is_active_sidebar('f_site_logo')){
						dynamic_sidebar('f_site_logo');
						}
						?>
						</a>
						</div>
						<div class="footer_column icon_or_link">
						<?php
						if(is_active_sidebar('icon_link')){
						dynamic_sidebar('icon_link');
						}
						?>
						</div>
					</div>
<div class="footer_devider">
				<div class="copyright_info">
				<p>Copyright &copy; <?php echo date('Y'); echo " "; echo get_bloginfo('name'); ?></p> 	
				<?php
				if(is_active_sidebar('terms_privacy')){
				dynamic_sidebar('terms_privacy');
				}
				?>		
				</div>
</div>

					</div>
				</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript">
jQuery(".menu-btn").click(function(){
	jQuery(".responsive-menu").slideToggle("slow");
	jQuery(".mobile-nav").toggleClass("closeicon");
});
	jQuery(window).scroll(function(){
  var sticky = jQuery('body'),
      scroll = jQuery(window).scrollTop();

  if (scroll >= 120) sticky.addClass('fixed');
  else sticky.removeClass('fixed');
});

</script>

<?php if(is_page($page = 88) || is_page($page = 89) ){ ?>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#signup").attr("disabled","true");
	});
	
		jQuery("#pwd").keyup(function() {
			$passlen = jQuery("#pwd").val().length;
			if($passlen < 6){
				jQuery('#errcnfpwd').remove();
				jQuery('#pwd').after('<p id="errcnfpwd" style="color:red;">Password must be contain at list 6 characters</p>');
				jQuery("#signup").attr("disabled","true");
			} else {
				jQuery('#errcnfpwd').remove();
				jQuery("#signup").removeAttr("disabled");
			}
		});

		jQuery("#cnfrmpwd").keyup(function() {
		$pass = jQuery("#pwd").val();
		
		$cnfpass = jQuery("#cnfrmpwd").val(); 
		$passlen
		

		if($pass != $cnfpass){
			jQuery('#errcnfpwd').remove();
			jQuery('#cnfrmpwd').after('<p id="errcnfpwd" style="color:red;">Password and Confirm Password doesn\'t match.</p>')
			
			jQuery("#signup").attr("disabled","true");
		}
		else
		{
			jQuery('#errcnfpwd').remove();
			jQuery("#signup").removeAttr("disabled");
		}
		});
</script>

<?php } ?>
<?php if(is_page($page = 30) || is_page($page = 9) ){ ?>
<script type="text/javascript">
	jQuery(document).ready(function(){
    var viewportWidth = jQuery(window).width();
    if (viewportWidth < 601) {
            jQuery(".responsive_view_image").insertAfter(".responsive_view_content")
    }
});	

	jQuery(window).resize(function () {
    var viewportWidth = jQuery(window).width();
    if (viewportWidth < 601) {
            jQuery(".responsive_view_image").insertAfter(".responsive_view_content")
    }else{
    	jQuery(".responsive_view_content").insertAfter(".responsive_view_image")
    }
});	

</script>
<?php } ?>

</body>
</html>
