<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package riggoh_company
 */

get_header();
?>

	<div id="primary" class="content-area">
	<div class="riggo_404">
		<div class="riggo_contnet_error">
		<h1>Oops!</h1>
		<h2>The page you were looking for doesn't exist.</h2>
		<a href="<?php echo get_site_URL();?>">Go to home</a>
		</div>
	</div>
		<main id="main" class="site-main">



		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
