<?php /* Template Name: template-quote */
get_header();

function add_datepicker_in_footer(){ 
wp_enqueue_script('jquery-ui-core');
wp_enqueue_script('jquery-ui-datepicker');
} add_action('wp_footer','add_datepicker_in_footer',10);

$miles = 0;
if(!empty($_POST)){
	$accesstokenarray = getAccessTokan();
	if(sizeof($accesstokenarray) > 1){
		$formualsarray = getFormulas($accesstokenarray['access_token']);
		$equipment = $_POST['equipment_group'];				
		$TargetRate = 'TL';
		$RateType = 'Flat';
		$data = $lane = array();
		$i = 0;
		if(empty($_POST['group'])){
			$data[0]['TypeOfEquipments'] = 'All';
			$data[0]['EquipmentGroup'] = $equipment;
			$data[0]['Mode'] = $TargetRate;
			$data[0]['LaneNumber'] = '';					
			$data[0]['Miles'] = '';
			$data[0]['OriginCity'] = $_POST['pickup_city'];
			$data[0]['OriginState'] =  $_POST['pickup_state'];
			$data[0]['OriginCountry'] = '';
			$data[0]['OriginZip'] = $_POST['pickup_zipcode'];
			$data[0]['DestinationCity'] = $_POST['city_del'];
			$data[0]['DestinationState'] = $_POST['state_del'];
			$data[0]['DestinationCountry'] =  '';
			$data[0]['DestinationZip'] = $_POST['zipcode_del'];
			$data[0]['RateType'] = $RateType;
		} else {
			$previousCity = null;
			$previousState = null;
			$previousZip = null;
			foreach ($_POST['group'] as $value) {
				$data[$i]['TypeOfEquipments'] = 'All';
				$data[$i]['EquipmentGroup'] = $equipment;
				$data[$i]['Mode'] = $TargetRate;
				$data[$i]['LaneNumber'] = '';						
				$data[$i]['Miles'] = '';
				$data[$i]['OriginCountry'] = '';
				if($previousCity) {
					$data[$i]['OriginCity'] = $previousCity;
				} else {
					$data[$i]['OriginCity'] = $_POST['pickup_city'];
				}
				if($previousState) {
					$data[$i]['OriginState'] = $previousState;
				} else {
					$data[$i]['OriginState'] =  $_POST['pickup_state'];
				}
				if($previousZip) {
					$data[$i]['OriginZip'] = $previousZip;
				} else {
					$data[$i]['OriginZip'] = $_POST['pickup_zipcode'];
				}
				$data[$i]['DestinationCity'] = $value['city_del'];
				$data[$i]['DestinationState'] = $value['state_del'];
				$data[$i]['DestinationZip'] = $value['zipcode_del'];

				$previousCity = $value['city_del'];
				$previousState = $value['state_del'];
				$previousZip = $value['zipcode_del'];
				$data[$i]['DestinationCountry'] =  '';
				$data[$i]['RateType'] = $RateType;
				$i++;
			}
		}
		
		//$lane = array('lane' => $data, 'CalculatedRateFormula' => '90 Day Rate', 'TimeFrameFromDate'=> date("m/d/Y",strtotime('-'.get_option("timeframedays").'days')), 'TimeFrameToDate'=> date("m/d/Y",strtotime('-1 days')));
		
		//Seven Day AVG = reports 10 or more
		//15 Day AVG = reports 30 or more
		//90 Day AVG = 
		$lane = array('lane' => $data, 'CalculatedRateFormula' => 'Seven Day AVG');		
		$output = json_encode($lane);
		$quoteresult = getRates($output,$accesstokenarray['access_token']);
		
		if($quoteresult['data'][0]->rateEngineResults[0]->reports < 10){
			$lane = array('lane' => $data, 'CalculatedRateFormula' => '15 Day AVG');		
			$output = json_encode($lane);
			$quoteresult = getRates($output,$accesstokenarray['access_token']);

			if($quoteresult['data'][0]->rateEngineResults[0]->reports < 30){
				$lane = array('lane' => $data, 'CalculatedRateFormula' => '90 Day AVG');		
				$output = json_encode($lane);
				$quoteresult = getRates($output,$accesstokenarray['access_token']);
			}
		}
		//echo "<pre>"; print_r($quoteresult); print_r($_POST); echo "</pre>";
		/*exit;*/
		$quote = 0;
		$miles = 0;				
		$marginper = get_option("commissionpercentage");
		if(sizeof($quoteresult['data']) >= 1){
			foreach ($quoteresult['data'] as $value) {
				$quote  += $value->allinRateFlat;
				$miles  += $value->miles;
			}
		}
	}	
}
if(!isset($_POST['date']) && $_POST['date']==""){
	$pickup_dates = date('M,d Y',strtotime("+1 day"));
} else {								
	$pickup_dates =  $_POST['date'];
}
?>
<div id="primary" class="content-area">
<main id="main" class="site-main quote-landing">
<section class="hero-section quote_banner">
<h1 class="freight-quote-title">Full Truck Load Freight Quote</h1>
<div class="freight-quote-main">
	<?php if($quoteresult['statusCode']>0){?>
	<div class="error_wrapper"><?php echo $quoteresult['message']?></div>
	<?php }?>
<div class="freight_form">
	<div class="quote-request-form">
		<form action="<?php echo get_site_url();?>/quote-landing/" name="quoterequest" id="quoterequest" method="post" class="repeater">
			<div class="form-top">
				<div class="form-top-left">
					<label class="pickupdate-label">Pickup Date</label>
					<input class="pickupdate" id="date" name="date" value="<?php echo $pickup_dates;?>" />
				</div>
				<div class="form-top-right">
					<label class="equipment-label">Equipment</label>
					<select class="equipment" name="equipment_group" id="equipment_group">
						<option <?php if ($_REQUEST['equipment_group']== "Van"){?> selected <?php } ?> value="Van">Dry Van</option>
						<option <?php if ($_REQUEST['equipment_group']== "Reefer"){?> selected <?php } ?> value="Reefer">Reefer</option>
					</select>
				</div>
			</div>
			<div class="form-bottom">
				<label class="pickup-label">Pickup</label>
				<div class="bottom_field">					
					<ul>
						<li><input type="text" class="googleauto pickupgrp emptyval" name="pic_address" value="<?php echo $_REQUEST['pic_address']; ?>" placeholder="Enter your address"><input type="hidden" class="emptyval" name="pickup_city" value="<?php echo $_REQUEST['pickup_city']; ?>"><input type="hidden" class="emptyval" name="pickup_state" value="<?php echo $_REQUEST['pickup_state']; ?>"></li>
						<li><input type="text" class="pickupgrp emptyval" name="pickup_zipcode" value="<?php echo $_REQUEST['pickup_zipcode']; ?>" placeholder="Zipcode"></li>
					</ul>
				</div>
				<div data-repeater-list='group'>
				<?php if(!empty($_POST['group'])){ for($d=0; $d<count($_POST['group']); $d++){?>
				<div class="data_main_repeater" data-repeater-item >
					<label class="delivery-label">Delivery</label>
					<div class="bottom_field">
						<ul>
							<li><input type="text" class="googleauto deleverygrp" name="dro_address" value="<?php echo $_POST['group'][$d]['dro_address']?>" placeholder="Enter your address"><input type="hidden" name="city_del" value="<?php echo $_POST['group'][$d]['city_del']?>" placeholder="City"><input type="hidden" name="state_del" value="<?php echo $_POST['group'][$d]['state_del']?>" placeholder="State"></li>
							<li><input type="text" class="deleverygrp" name="zipcode_del" value="<?php echo $_POST['group'][$d]['zipcode_del']?>" placeholder="Zipcode"></li>
						</ul>
						<input class="delete_cell" data-repeater-delete type="button" value="Delete"/>
					</div>
				</div>
				<?php } } else {?>
				<div class="data_main_repeater" data-repeater-item >
					<label class="delivery-label">Delivery</label>
					<div class="bottom_field">
						<ul>
							<li><input type="text" class="googleauto deleverygrp" name="dro_address" value="<?php echo $_REQUEST['del_address']; ?>" placeholder="Enter your address"><input type="hidden" name="city_del" value="<?php echo $_REQUEST['city_del']; ?>" placeholder="City"><input type="hidden" name="state_del" value="<?php echo $_REQUEST['state_del']; ?>" placeholder="State"></li>
							<li><input type="text" class="deleverygrp" name="zipcode_del" value="<?php echo $_REQUEST['zipcode_del']; ?>" placeholder="Zipcode"></li>
						</ul>
						<input class="delete_cell" data-repeater-delete type="button" value="Delete"/>
					</div>
				</div>
					
				
					
				<?php }?>
					
				<!-- <div class="data_main_repeater" data-repeater-item >
					<label class="delivery-label">Delivery</label>
					<div class="bottom_field">
						<ul>
							<li><input type="text" class="googleauto deleverygrp" name="dro_address" value="<?php echo $_REQUEST['del_address']; ?>" placeholder="Enter your address"><input type="hidden" name="city_del" value="<?php echo $_REQUEST['city_del']; ?>"><input type="hidden" name="state_del" value="<?php echo $_REQUEST['state_del']; ?>"></li>
							<li><input type="text" class="deleverygrp" name="zipcode_del" value="<?php echo $_REQUEST['zipcode_del']; ?>" placeholder="Zipcode"></li>
						</ul>
						<input class="delete_cell" data-repeater-delete type="button" value="Delete"/>
					</div>
				</div> -->
				<!--<input class="add_cell" data-repeater-create type="button" value="Add"/>-->
				</div>
				
				<!-- commodity field -->	
				<div class="data_main_repeater" data-repeater-item >
					<label class="commodity-label">Commodity</label>
					<div class="bottom_field">
						<ul>
							<li><input type="text" class="commodity" name="commodity" id="commodity" value="<?php echo $_REQUEST['commodity']; ?>" placeholder="Enter your commodity" data-rule-required="false" maxlength="50">
						</ul>
					</div>
				</div>	
					<!-- commodity field -->
				
				<!-- email field -->	
				<div class="data_main_repeater" data-repeater-item >
					<label class="email-label">Email Address</label>
					<div class="bottom_field">
						<ul>
							<li><input type="text" class="email_address" name="email_address" id="email_address" value="<?php echo $_REQUEST['email_address']; ?>" placeholder="Enter your email address" data-rule-required="false" data-rule-email="true" data-msg-email="Please enter a valid email address">
						</ul>
					</div>
				</div>	
					<!-- email field -->
				
				
				
					<div class="btn_gradient">
					<input type="submit" name="submit" value="Submit">
					</div>
			</div>
		</form>		
	</div>
</div>
<div class="freight_info">
	<div class="quote-request-output">
		<div class="quote-request-estimation">
			<?php 
			function getUserIpAddr(){
    			if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        		//ip from share internet
        		$ip = $_SERVER['HTTP_CLIENT_IP'];
    			}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        		//ip pass from proxy
        		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    			}else{
        		$ip = $_SERVER['REMOTE_ADDR'];
    			}
    			return $ip;
			}
			
			
			
			if($quote <= 0){?>
				<span class="estimated-time-label">Please contact us for a quote</span>
				<span class="estimated-time-value"><a href="mailto:sales@riggo.io">sales@riggo.io</a> <br /><a href="tel:646-779-0460">646-779-0460</a></span>
			<?php } else {
				$quoteper = $quote /(1- ($marginper)/100);
				$finalquote = $quoteper;

				global $wpdb;	
				$table_name = $wpdb->prefix . 'freight_quotes';	
				if($wpdb->get_var("SHOW TABLES LIKE '$table_name'")){					
					if(!isset($_POST['date']) && $_POST['date']==""){
						$pickup_date = date('Y-m-d',strtotime("+1 day"));
					} else {						
						$strdate = str_replace(",", "-", str_replace(" ","-",$_POST['date']));
						$pickup_date =  date("Y-m-d",strtotime($strdate));
					}					
					$equipment = $_POST['equipment_group'];
					$pickup_city = $_POST['pickup_city'];
					$pickup_state = $_POST['pickup_state'];
					$pickup_zipcode = $_POST['pickup_zipcode'];
					$pic_address = $_POST['pic_address'];
					$email_address = $_POST['email_address'];
					$commodity = $_POST['commodity'];
					$ip_address = getUserIpAddr();
					$user_agent = $_SERVER['HTTP_USER_AGENT'];
					if(empty($_POST['group'])){						
						$dro_address = $_POST['del_address'];
						$city_del = $_POST['city_del'];
						$state_del = $_POST['state_del'];
						$zipcode_del = $_POST['zipcode_del'];
					} else {						
						$dro_address = $_POST['group'][0]['dro_address'];
						$city_del = $_POST['group'][0]['city_del'];
						$state_del = $_POST['group'][0]['state_del'];
						$zipcode_del = $_POST['group'][0]['zipcode_del'];
					}

					$wpdb->insert( 
						$table_name, 
						array(
							'pickup_date' => $pickup_date,
							'equipment' => $equipment,
							'pickup_city' => $pickup_city,
							'pickup_state' => $pickup_state,
							'pickup_zipcode' => $pickup_zipcode,
							'pic_address' => $pic_address,
							'dro_address' => $dro_address,
							'city_del' => $city_del,
							'state_del' => $state_del,
							'zipcode_del' => $zipcode_del,
							'est_quote' => $finalquote, 
							'est_miles' => $miles, 
							'email_address'=> $email_address,
							'commodity'=> $commodity,
							'created_date' => current_time( 'mysql' ), 
							'ip_address' => $ip_address,
							'user_agent' => $user_agent,
						) 
					);
				}
			?>
				<!-- <span class="estimated-price">$<?php //echo number_format($quote / (1-($marginper)/100),2);?></span> -->
				<span class="estimated-price">$<?php echo number_format($finalquote,2);?></span>
				<span class="estimated-time-label">Estimated Distance</span>
				<span class="estimated-time-value"><?php echo $miles?> miles</span>
			<?php }?>
			<div class="action_btn btn_gradient"><a class="btn_border btn_gradient" href="<?php echo get_site_url();?>/shipper_form/">Sign up and ship</a></div>
		</div>
	</div>
</div>
	


</div>
</section>
<script type='text/javascript' src='<?php echo get_site_url();?>/wp-content/themes/riggoh/js/jquery.repeater.js'></script>
<script>
    jQuery(document).ready(function ($) {
        'use strict';

        $('.repeater').repeater({
            defaultValues: {
                'city': '',
            },
            show: function () {
                $(this).slideDown();
                var inputs = document.getElementsByClassName('googleauto');
		      	for (var i = 0; i < inputs.length; i++) {
		        	initialize(inputs[i]);
		      	}
		      	var clssname = 'deleverygrp'+$('.data_main_repeater').length;
		      	$(this).find('.googleauto').removeClass('deleverygrp').addClass(clssname);
		      	$(this).find('.deleverygrp').removeClass('deleverygrp').addClass(clssname);		      	

		      	jQuery('.'+clssname).each(function () {					
				    jQuery(this).rules('add', {
				        require_from_group: [1, '.'+clssname]
				    });
				});
            },
            hide: function (deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                    var inputs = document.getElementsByClassName('googleauto');
			        for (var i = 0; i < inputs.length; i++) {
			         initialize(inputs[i]);
			        }
                }
            },
            ready: function (setIndexes) {
			var inputs = document.getElementsByClassName('googleauto');
			      for (var i = 0; i < inputs.length; i++) {
			        initialize(inputs[i]);
			      }
            }
        });
	});
    </script><!---->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlGYq480E9K5fc08ATnvOxNz31XoDJtj0&libraries=places"></script>
<script type="text/javascript">
  function fillInAddress(show,ac) {
  					var name = show.getAttribute('name');
                    var place = ac.getPlace();
                    for (var i = 0; i < place.address_components.length; i++){
                      if(place.address_components[i].types[0] == 'locality'){
                          var city = place.address_components[i].short_name;
                      }
                      if(place.address_components[i].types[0] == 'sublocality_level_1'){
						  var city1 = place.address_components[i].short_name;
						}
                      if(place.address_components[i].types[0] == 'administrative_area_level_1'){
                          var state = place.address_components[i].short_name;
                      }
                      /*if(place.address_components[i].types[0] == 'postal_code'){
                          var postalcode = place.address_components[i].short_name;
                      }*/
                    }
                    if(city){
                    	jQuery('input[name="'+name+'"]').next().val(city);
                    } else if(city1){  
                    	jQuery('input[name="'+name+'"]').next().val(city1);
                    }
                    if(state){
                    	jQuery('input[name="'+name+'"]').next().next().val(state);
                	}
                 /*   if(postalcode){
                    	jQuery('input[name="'+name+'"]').parent().next().find(':input').val(postalcode);
                    }*/
                }

        function initialize(out) {
           var  autocomplete = new google.maps.places.Autocomplete(
                    (out),
                    {types: ['geocode']});

            google.maps.event.addListener(autocomplete, 'place_changed', 
               function () {
                fillInAddress(out,autocomplete);
            });
        }
      var inputs = document.getElementsByClassName('googleauto');
      for (var i = 0; i < inputs.length; i++) {
        initialize(inputs[i]);
      }
 </script>
		<?php
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content', 'page' );
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
		endwhile; // End of the loop.
		?>
		</main><!-- #main -->
	</div><!-- #primary -->
<script src="<?php echo get_template_directory_uri()?>/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/additional-methods.min.js"></script>
<script>
/*jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});*/

jQuery.validator.addMethod("require_from_group", jQuery.validator.methods.require_from_group, 'Pick at least one');
jQuery( "#quoterequest" ).validate({	
  rules: {
    pic_address: {
      require_from_group: [1, ".pickupgrp"]
    },
    pickup_zipcode: {
      require_from_group: [1, ".pickupgrp"]
    },
    "group[0][dro_address]": {
      require_from_group: [1, ".deleverygrp"]
    },
    "group[0][zipcode_del]": {
      require_from_group: [1, ".deleverygrp"]
    }
  }
});

</script>	
<?php 
function getAccessTokan(){
		$usename = TRUCKSTOP_USERNAME;
		$password = TRUCKSTOP_PASSWORD;
		$accesstoken = TRUCKSTOP_CLIENTID.":".TRUCKSTOP_CLIENTSECRET;
		$base64_encode = base64_encode($accesstoken);
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => TRUCKSTOP_ACCESSTOKENAPIURL,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "grant_type=password&username=$usename&password=$password&undefined=",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: basic $base64_encode",
		    "Content-Type: application/x-www-form-urlencoded",
		    "Postman-Token: a7f95d48-9581-44e8-a276-3c65f2419683",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  return (array) json_decode($err);
		} else {
		  return (array) json_decode($response);
		}
}

function getFormulas($token){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => TRUCKSTOP_FORMULASAPIURL,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer $token",
		    "Content-Type: application/json",
		    "Postman-Token: 65f0ff58-ecb9-4af3-8a1a-1406d0bad858",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  return (array) json_decode($err);
		} else {
		  return (array) json_decode($response);
		}
}

function getRates($data,$token){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => TRUCKSTOP_QUOTEAPIURL,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $data,
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: Bearer $token",
		    "Content-Type: application/json",
		    "Postman-Token: fa1f511c-8231-416a-91f4-bef260ffa4a2",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return (array) json_decode($err);
		} else {
		  return (array) json_decode($response);
		}	
}
?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('#date').datepicker({
        dateFormat: 'M,dd yy',
        minDate: 0
    });
    //jQuery("#date").datepicker("setDate", "1");
});
</script>
<?php
get_sidebar();
get_footer();
?>