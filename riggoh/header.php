<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package riggoh_company
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'riggoh' ); ?></a>

<div class="main_header">
	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$riggoh_description = get_bloginfo( 'description', 'display' );
			if ( $riggoh_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $riggoh_description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->


 <!-- Mobile menu -->
<div class="mobile-nav" id="nav-main-mobile">
    <div class="menu-btn-container">
     <div id="menu-btn" class="menu-btn">
  <span></span>
  <span></span>
  <span></span>
</div>
    </div>
  </div>
     <div class="responsive-menu">
        <div class="button_wrapper">
			<?php wp_nav_menu( array( 'theme_location' => 'new-menu' ) ); ?>
		</div>
        <?php
	    $mobile_nav = array(
	        'theme_location'  => 'menu-1',
	        'menu'            => '',
	        'container'       => 'div',
	        'container_class' => 'menu-header',
	        'container_id'    => '',
	        'menu_class'      => 'menu',
	        'menu_id'         => 'primary-menu',
	        'echo'            => true,
	        'fallback_cb'     => 'wp_page_menu',
	        'before'          => '',
	        'after'           => '',
	        'link_before'     => '',
	        'link_after'      => '',
	        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
	        'depth'           => 0,
	        'walker'          => ''
	    );
        wp_nav_menu( $mobile_nav );        ?>
     </div>



		<nav id="site-navigation" class="main-navigation 111">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'riggoh' ); ?></button>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu--1',
			) );
			?>
			<div class="button_wrapper">
				<?php wp_nav_menu( array( 'theme_location' => 'new-menu' ) ); ?>
			</div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
</div>

	<div id="content" class="site-content">
