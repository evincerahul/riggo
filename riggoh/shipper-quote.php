<?php /* Template Name: Shipper-quote */
get_header();
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main quote-landing">
		<section class="hero-section quote_banner shipper_main_quote">
			<div class="freight-quote-main">
				<div class="freight_info">
					<div class="quote-request-output">
						<div class="home-page-banner">
							<h1><?php echo get_field( "shipper_title" );?></h1>
							<p><?php echo get_field( "shipper_description" );?></p>
							<div class="talk_to_representative"><?php echo get_field( "representative_text" );?><br>
								<a href="tel:<?php echo get_field( "shipper_phone" );?>"><?php echo get_field( "shipper_phone" );?></a></div>
							</div>
						</div>
					</div>
					<div class="freight_form">
						<div class="quote-request-form">
							<form name="quoterequest" id="quoterequest" action="<?php echo get_site_url();?>/quote-landing/" method="post" class="repeater">
								<div class="form-bottom">
									<h2>Full Truck Load Freight Quote</h2>
									<label class="pickup-label bold_text">Pick up Address</label>
									<div class="bottom_field">
										<ul>
											<li><input type="text" class="googleauto pickupgrp" name="pic_address" id="pic_address" value="" placeholder="Enter your address"><input type="hidden" name="pickup_city" value=""><input type="hidden" name="pickup_state" value=""></li>
											<!-- <li><input type="text" name="state" value="" placeholder="State"></li> -->
											<li><input type="text" class="pickupgrp" name="pickup_zipcode" id="pickup_zipcode" value="" placeholder="Zipcode"></li>
										</ul>
									</div>
									<div class="data_main_repeater" data-repeater-item >
										<label class="delivery-label">Delivery Address</label>
										<div class="bottom_field">
											<ul>
												<li><input type="text" class="googleauto deleverygrp" name="del_address" id="del_address" value="" placeholder="Enter your address"><input type="hidden" name="city_del" value=""><input type="hidden" name="state_del" value=""></li>
												<!-- <li><input type="text" name="state_del" value="" placeholder="State"></li> -->
												<li><input type="text" class="deleverygrp" name="zipcode_del" id="zipcode_del" value="" placeholder="Zipcode"></li>
											</ul>					
										</div>

									</div>
									
									<!--commodity field -->
									<div class="data_main_repeater" data-repeater-item >
										<label class="commodity-label">Commodity</label>
										<div class="bottom_field">
											<ul>
												<li><input type="text" class="commodity" name="commodity" id="commodity" value="" placeholder="Enter your commodity" data-rule-required="false" maxlength="50"></li>
											</ul>					
										</div>

									</div>
									<!--commodity field-->
									
									<!--email field -->
									<div class="data_main_repeater" data-repeater-item >
										<label class="email-label">Email Address</label>
										<div class="bottom_field">
											<ul>
												<li><input type="text" class="email_address" name="email_address" id="email_address" value="" placeholder="Enter your email address" data-rule-required="false" data-rule-email="true" data-msg-email="Please enter a valid email address"></li>
											</ul>					
										</div>

									</div>
									<!--email field-->
									

									<div class="radio_btn">
										<div class="pretty p-default p-round">
											<input type="radio" name="equipment_group" value="Van" checked>
											<div class="state">
												<label>Dry Van</label>
											</div>
										</div>
										<div class="pretty p-default p-round">
											<input type="radio" name="equipment_group" value="Reefer">
											<div class="state">
												<label>Reefer</label>
											</div>
										</div>
									</div>
									<div class="btn_gradient">
										<input type="submit" name="submit" value="Quote">
									</div>

								</div>
							</form>		
						</div>
					</div>
				</div>
			</section>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlGYq480E9K5fc08ATnvOxNz31XoDJtj0&libraries=places"></script>
			<script type="text/javascript">
				function fillInAddress(show,ac) {
					var name = show.getAttribute('name');
					var place = ac.getPlace();
					for (var i = 0; i < place.address_components.length; i++){
						if(place.address_components[i].types[0] == 'locality'){
							var city = place.address_components[i].short_name;
						}
						if(place.address_components[i].types[0] == 'sublocality_level_1'){
							var city1 = place.address_components[i].short_name;
						}

						if(place.address_components[i].types[0] == 'administrative_area_level_1'){
							var state = place.address_components[i].short_name;
						}
						/*if(place.address_components[i].types[0] == 'postal_code'){
						  var postalcode = place.address_components[i].short_name;
						}*/
					}
					if(city){
						jQuery('input[name="'+name+'"]').next().val(city);
					} else if(city1){  
						jQuery('input[name="'+name+'"]').next().val(city1);
					}
					if(state){
						jQuery('input[name="'+name+'"]').next().next().val(state);
					}
                    /*if(postalcode){
                    	jQuery('input[name="'+name+'"]').parent().next().find(':input').val(postalcode);
                    }*/
                }

                function initialize(out) {
                	var  autocomplete = new google.maps.places.Autocomplete(
                		(out),
                		{types: ['geocode']});

                	google.maps.event.addListener(autocomplete, 'place_changed', 
                		function () {
                			fillInAddress(out,autocomplete);
                		});
                }
                var inputs = document.getElementsByClassName('googleauto');
                for (var i = 0; i < inputs.length; i++) {
                	initialize(inputs[i]);
                }

                jQuery('#quoterequest .radio_btn input:radio').click(function () {
                	jQuery('input:radio').parent().removeClass('checked11');
                	jQuery(this).parent(this).addClass('checked11');
                });
                var input = document.getElementById('pic_address');                
				google.maps.event.addDomListener(input, 'keydown', function(event) { 					
				    if (event.keyCode === 13) { 
				        event.preventDefault(); 
				    }
				});
				var input1 = document.getElementById('del_address');                
				google.maps.event.addDomListener(input1, 'keydown', function(event) { 					
				    if (event.keyCode === 13) { 
				        event.preventDefault(); 
				    }
				}); 
            </script>

            <?php
            while ( have_posts() ) :
            	the_post();
            	get_template_part( 'template-parts/content', 'page' );
			// If comments are open or we have at least one comment, load up the comment template.
            	if ( comments_open() || get_comments_number() ) :
            		comments_template();
            	endif;
		endwhile; // End of the loop.
		?>
	</main><!-- #main -->
</div><!-- #primary -->
<script src="<?php echo get_template_directory_uri()?>/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/additional-methods.min.js"></script>

<script>
/*jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});*/
jQuery(document).ready(function(){
	jQuery.validator.addMethod("require_from_group", jQuery.validator.methods.require_from_group, 'Pick at least one');
	jQuery( "#quoterequest" ).validate({
		ignore: ".ignore",
		rules: {
			pic_address: {
				require_from_group: [1, ".pickupgrp"]
			},
			pickup_zipcode: {
				require_from_group: [1, ".pickupgrp"]
			},
			del_address: {
				require_from_group: [1, ".deleverygrp"]
			},
			zipcode_del: {
				require_from_group: [1, ".deleverygrp"]
			}
		}
	});
});
</script>
<?php
get_sidebar();
get_footer();
?>
